from django.contrib import admin
from django.urls import path
from django.urls.conf import include

from .views import ArticleListView, ArticleDetailView


urlpatterns = [
    path('', ArticleListView.as_view()),
    path('<pk>', ArticleDetailView.as_view())
]
